<?php

use Imms\Classes\Bootstrapper;
use Imms\Classes\Route;
use League\Flysystem\FilesystemException;

$rootDir = Bootstrapper::rootDirectory();
$ini = Bootstrapper::getIni();
$mdPath = $ini['app']['md_path'];
try {
	[$cnt, $html] = Route::createMenu($mdPath);
	echo $html;
} catch (FilesystemException $e) {
	echo $e->getMessage();
}
